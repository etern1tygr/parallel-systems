#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define GRID_COLUMNS   840             // x dimension of problem grid
#define GRID_ROWS      840         // y dimension of problem grid
#define GENERATIONS  300                // number of time steps
#define MASTER      0                   // taskid of first process
#define UP      0
#define DOWN    1
#define LEFT    2
#define RIGHT   3
#define REORDER 1
#define REDUCE 20    // run reduce after this number of steps
#define REDUCE_ENABLED 0  //enable reduction


/*---------------------- Inline Function Declarations ---------------------------*/

int x, y;
inline void updateInternal(int columns, int rows, int *u1, int *u2);
inline void updateBorders(int columns, int rows, int *u1, int *u2);
inline int gensEqual(int columns, int rows, int *u1, int *u2);
inline int genAllZero(int columns, int rows, int *u);
inline void initializeGrid(int columns, int rows, int *u);
inline void printFile(int columns, int rows, int *u1, char *fnam);
inline void printConsole(int columns, int rows, int *u);

            /*---------------------- Main ---------------------------*/

int main(int argc, char *argv[]) {

    srand(time(NULL));
    double start, finish;    // Timestamps
    int *u1, *u2, *U;       // u1, u2 arrays for processes blocks | U array used to initialize data, send blocks to workers and then receive blocks
    int rank;       // Process/Task unique id
    int N;      // Number of Processes/Tasks
    int blockRows, blockColumns;    // Number of process block rows and columns
    int sideBlocks;                 // Number of blocks for each grid side = vertical || horizontal
    int offsetX, offsetY;       // Used to send rows of data from the grid
    int dims[2], periods[2];        // MPI_Cart_Create parameters
    int neighbors[4];       // A simple 4-integer array for each top, bottom, left, right neighbor of each task
    int extendedBlockColumns, extendedBlockRows;  // Extended block rows and columns to include extra lines for border calculation
    int i, swap, it;       // Iterator variables
//    int *temp;              // Temp pointer to integer array used in swapping of u1 and u2 arrays
//    int reduce1, reduce2, isReduce, reduction;  // Reduction Variables




        /*------------------------------ MPI Topology Initializations -----------------------*/

    MPI_Comm cartComm;
    MPI_Request recvRequests[2][4], sendRequests[2][4];     // Used in MPI_Recv_init and MPI_Send_init
    MPI_Init(&argc, &argv);                 // Initialize mpi
    MPI_Comm_size(MPI_COMM_WORLD, &N);      // Give number of tasks communication topology
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);   // Give ranks to communication topology

    sideBlocks = sqrt(N);
    blockColumns = GRID_COLUMNS / sideBlocks;
    blockRows = GRID_ROWS / sideBlocks;
    extendedBlockColumns = blockColumns + 2;
    extendedBlockRows = blockRows + 2;

    printf("Initializing process with rank is %d / size: %d, block rows: %d block cols:%d \n", rank, N, blockRows, blockColumns);

    dims[0] = dims[1] = sideBlocks;
    periods[0] = periods[1] = 1;    // 1 for periodic array
    MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, REORDER, &cartComm);  // Cartesian Topology Creation


    MPI_Cart_shift(cartComm, 0, 1, &neighbors[UP], &neighbors[DOWN]);       // Find neighbors for each process with shifting
    MPI_Cart_shift(cartComm, 1, 1, &neighbors[LEFT], &neighbors[RIGHT]);

    //printf("Rank:%d neigbours: %d %d %d %d \n", rank, neighbors[UP], neighbors[DOWN], neighbors[LEFT], neighbors[RIGHT]);


        /*-------------------------------------- DATATYPES -------------------------------------*/


    MPI_Datatype masterToWorker, workerToMaster, columnDatatype, rowDatatype;

    // Datatype used by master to send and receive blocks to/from worker processes
    MPI_Type_vector(blockRows, blockColumns, GRID_COLUMNS, MPI_INT, &masterToWorker);
    MPI_Type_commit(&masterToWorker);

    // Datatype used by workers to send and receive blocks to/from master process
    MPI_Type_vector(blockRows, blockColumns, extendedBlockColumns, MPI_INT, &workerToMaster);
    MPI_Type_commit(&workerToMaster);

    //datatype to send one column
    MPI_Type_vector(blockRows, 1, extendedBlockColumns, MPI_INT, &columnDatatype);
    MPI_Type_commit(&columnDatatype);

    //datatype to send one row
    MPI_Type_vector(1, blockColumns, extendedBlockColumns, MPI_INT, &rowDatatype);
    MPI_Type_commit(&rowDatatype);


    u1 = malloc(extendedBlockColumns * extendedBlockRows * sizeof (int));
    u2 = malloc(extendedBlockColumns * extendedBlockRows * sizeof (int));

       /*-------------------- Split, Send and Receive Grid Blocks  ----------------------------*/

    if (rank == MASTER) {
        U = malloc(sizeof (int) * GRID_COLUMNS * GRID_ROWS);    // Allocate grid
        initializeGrid(GRID_COLUMNS, GRID_ROWS, U);             // Initialize whole grid with 0s and 1s
	//printConsole(GRID_COLUMNS, GRID_ROWS, U);
        for (y = 1; y < extendedBlockRows-1 ; y++){    // Master sends to itself to be a worker too
            for (x = 1; x < extendedBlockColumns-1 ; x++){
                *(u1 + y * extendedBlockColumns + x) = *(U + (y-1) * GRID_COLUMNS + (x-1));
            }
        }
	//printConsole(extendedBlockColumns,extendedBlockRows,u1);

        offsetX = offsetY = 0;
        for (i = 1; i < N; i++) {   // Send blocks to worker processes
            offsetX += blockColumns;
            if (offsetX == GRID_COLUMNS ) {
                offsetX = 0;
                offsetY += blockRows;
            }
            MPI_Send(&offsetX, 1, MPI_INT, i, 0, cartComm);
            MPI_Send(&offsetY, 1, MPI_INT, i, 0, cartComm);
            MPI_Send(U + offsetY * GRID_COLUMNS + offsetX, 1, masterToWorker, i, 0, cartComm);
        }
        offsetX = offsetY = 0;
    } else{
        MPI_Recv(&offsetX, 1, MPI_INT, MASTER, 0, cartComm, MPI_STATUS_IGNORE);     // Receive Blocks from master process
        MPI_Recv(&offsetY, 1, MPI_INT, MASTER, 0, cartComm, MPI_STATUS_IGNORE);
        MPI_Recv(u1 + extendedBlockColumns + 1, 1, workerToMaster, MASTER, 0, cartComm, MPI_STATUS_IGNORE);
	//printConsole(extendedBlockColumns,extendedBlockRows,u1);
    }



                /*--------------- Block Border Send and Receive Initializations for u1 ----------------*/
    //top row
    MPI_Send_init(u1 + extendedBlockColumns + 1, 1, rowDatatype, neighbors[UP], 0, cartComm, &sendRequests[0][0]);
    //bottom row
    MPI_Send_init(u1 + (extendedBlockRows - 2) * extendedBlockColumns + 1, 1, rowDatatype, neighbors[DOWN], 0, cartComm, &sendRequests[0][1]);
    //right column
    MPI_Send_init(u1 + 2 * extendedBlockColumns - 2, 1, columnDatatype, neighbors[RIGHT], 0, cartComm, &sendRequests[0][2]);
    //left column
    MPI_Send_init(u1 + extendedBlockColumns + 1, 1, columnDatatype, neighbors[LEFT], 0, cartComm, &sendRequests[0][3]);

    //top row
    MPI_Recv_init(u1 + 1, 1, rowDatatype, neighbors[UP], 0, cartComm, &recvRequests[0][0]);
    //bottom row
    MPI_Recv_init(u1 + (extendedBlockRows - 1) * extendedBlockColumns + 1, 1, rowDatatype, neighbors[DOWN], 0, cartComm, &recvRequests[0][1]);
    //right column
    MPI_Recv_init(u1 + 2 * extendedBlockColumns - 1, 1, columnDatatype, neighbors[RIGHT], 0, cartComm, &recvRequests[0][2]);
    //left column
    MPI_Recv_init(u1 + extendedBlockColumns, 1, columnDatatype, neighbors[LEFT], 0, cartComm, &recvRequests[0][3]);


                /*--------------- Block Border Send and Receive Initializations for u2 ----------------*/
    //top row
    MPI_Send_init(u2 + extendedBlockColumns + 1, 1, rowDatatype, neighbors[UP], 0, cartComm, &sendRequests[1][0]);
    //bottom row
    MPI_Send_init(u2 + (extendedBlockRows - 2) * extendedBlockColumns + 1, 1, rowDatatype, neighbors[DOWN], 0, cartComm, &sendRequests[1][1]);
    //right column
    MPI_Send_init(u2 + 2 * extendedBlockColumns - 2, 1, columnDatatype, neighbors[RIGHT], 0, cartComm, &sendRequests[1][2]);
    //left column
    MPI_Send_init(u2 + extendedBlockColumns + 1, 1, columnDatatype, neighbors[LEFT], 0, cartComm, &sendRequests[1][3]);

    //top row
    MPI_Recv_init(u2 + 1, 1, rowDatatype, neighbors[UP], 0, cartComm, &recvRequests[1][0]);
    //bottom row
    MPI_Recv_init(u2 + (extendedBlockRows - 1) * extendedBlockColumns + 1, 1, rowDatatype, neighbors[DOWN], 0, cartComm, &recvRequests[1][1]);
    //right column
    MPI_Recv_init(u2 + 2 * extendedBlockColumns - 1, 1, columnDatatype, neighbors[RIGHT], 0, cartComm, &recvRequests[1][2]);
    //left column
    MPI_Recv_init(u2 + extendedBlockColumns, 1, columnDatatype, neighbors[LEFT], 0, cartComm, &recvRequests[1][3]);

    swap = 0;
    MPI_Barrier(cartComm);      // Barrier to wait of all processes to start together

                /*----------- Beginning Generation Time Steps - Critical Section ------------*/

    int *temp;
    int reduce1,reduce2,isReduce,reduction;

    printf("Rank %d received work. Beginning time steps...\n", rank);

    if (rank == MASTER)
        start = MPI_Wtime();            // Get the starting timestamp


    for (it = 1; it <= GENERATIONS; it++) {


        MPI_Startall(4, &sendRequests[swap][0]);    // Send borders
	//printf("my rank is: %d \n",rank);
        updateInternal(extendedBlockColumns, extendedBlockRows, u1, u2);    // Update the internal of the block

        MPI_Startall(4, &recvRequests[swap][0]);    // Receive borders

        MPI_Waitall(4, &recvRequests[swap][0], MPI_STATUSES_IGNORE);    // Wait for all to receive

        updateBorders(extendedBlockColumns, extendedBlockRows, u1, u2);     // Update the borders since you have received

        MPI_Waitall(4, &sendRequests[swap][0], MPI_STATUSES_IGNORE);    // Wait for all to send


            /*----------------- Swap Arrays --------------------*/

        temp = u1;          // Swapping Arrays Saves time and memory instead of recreating arrays for each generation
        u1 = u2;
        u2 = temp;
        swap = 1 - swap;

        //reduction

        if(it % REDUCE == 0 && REDUCE_ENABLED == 1) {
			reduce1 = gensEqual(extendedBlockColumns, extendedBlockRows, u1, u2);  // 1st reduce condition
            reduce2 = genAllZero(extendedBlockColumns, extendedBlockRows, u1);      // 2nd reduce condition
            isReduce = reduce1 || reduce2;                                          // If a reduce condition exists
            MPI_Allreduce(&isReduce, &reduction, 1, MPI_INT, MPI_LOR, cartComm);    // Reduce
            if(reduction == 0) {
                break;
            }
        }
    }


            /*------------------ Rebuild the final entire Grid U ----------------------*/

    if (rank != MASTER) {
        MPI_Send(&offsetX, 1, MPI_INT, MASTER, 0, cartComm);        // Send each process portion of final block results back to master
        MPI_Send(&offsetY, 1, MPI_INT, MASTER, 0, cartComm);
        MPI_Send(u1 + extendedBlockColumns + 1, 1, workerToMaster, MASTER, 0, cartComm);
    }


    if (rank == MASTER) {                   // Now wait for results from all worker tasks

        finish = MPI_Wtime();
        printf("Elapsed time = %f seconds\n", finish - start);  //print total computation time

        //receive all blocks
        for (i = 1; i < N; i++) {
            MPI_Recv(&offsetX, 1, MPI_INT, i, 0, cartComm, MPI_STATUS_IGNORE);
            MPI_Recv(&offsetY, 1, MPI_INT, i, 0, cartComm, MPI_STATUS_IGNORE);
            MPI_Recv(U + offsetY * GRID_COLUMNS + offsetX, 1, masterToWorker, i, 0, cartComm, MPI_STATUS_IGNORE);
        }

        for (y = 1; y < extendedBlockRows-1; y++){    // Copy master's block to U array
            for (x = 1; x < extendedBlockColumns-1; x++){
                *(U + (y-1) * GRID_COLUMNS + (x-1)) = *(u1 + y * extendedBlockRows + x);
            }
        }
	//printConsole(GRID_COLUMNS, GRID_ROWS, U);
        free(U);
    }

    free(u1);
    free(u2);
    MPI_Finalize();

    return 0;
}




    /*----------------- Initialization Functions --------------------*/
void initializeGrid(int columns, int rows, int *u) { // initialize data
    int y, x;

    for (y = 0; y <= rows - 1; y++)
        for (x = 0; x <= columns - 1; x++)
            *(u + y * columns + x) = rand() % 2;    //this initializes grid with 0s and 1s representing alive and dead cells

}

    /*----------------------- Block Update Functions --------------------------*/

void updateInternal(int columns, int rows, int *u1, int *u2) {  //update internal grid of process block
    int x, y;
    int i, j;
    int c;

    for (y = 2; y < rows - 2; y++) {
        for (x = 2; x < columns - 2; x++) {
            int pivot = y * columns + x;
            int cell = *(u1 + pivot);

            int count = 0;
            for (i = -1; i <= +1; i++) {
                for (j = -1; j <= +1; j++) {
                    if (i == 0 && j == 0) {
                        continue;
                    }

                    c = *(u1 + pivot + (i * columns) + j);

                    if (c == 1) {
                        count++; //count of alive neighbors
                    }
                }
            }
	//printf(" y:%d x:%d count:%d \n",y,x,count);
            if (count < 2)
                *(u2 + pivot) = 0;
            else if ((count == 2 || count == 3) && cell == 1)
                *(u2 + pivot) = 1;
            else if (count > 3 && cell == 1)
                *(u2 + pivot) = 0;
            else if (count == 3 && cell == 0)
                *(u2 + pivot) = 1;
        }
    }
}



void updateBorders(int columns, int rows, int *u1, int *u2) {
    int x, y;
    int i, j;
    int c;

    for (x = 1, y = 1; x <= columns - 2; x++) {
        int pivot = y * columns + x;
        int cell = *(u1 + y * columns + x);

        int count = 0;
        for (i = -1; i <= +1; i++) {
            for (j = -1; j <= +1; j++) {
                if (i == 0 && j == 0) {
                    continue;
                }

                c = *(u1 + pivot + (i * columns) + j);

                if (c == 1) {
                    count++; //count of alive neighbors + current cell if alive
                }
            }
        }

        if (count < 2)
            *(u2 + y * columns + x) = 0;
        else if ((count == 2 || count == 3) && cell == 1)
            *(u2 + y * columns + x) = 1;
        else if (count > 3 && cell == 1)
            *(u2 + y * columns + x) = 0;
        else if (count == 3 && cell == 0)
            *(u2 + y * columns + x) = 1;
    }

    for (x = 1, y = rows-2; x <= columns - 2; x++) {
        int pivot = y * columns + x;
        int cell = *(u1 + y * columns + x);

        int count = 0;
        for (i = -1; i <= +1; i++) {
            for (j = -1; j <= +1; j++) {
                if (i == 0 && j == 0) {
                    continue;
                }

                c = *(u1 + pivot + (i * columns) + j);

                if (c == 1) {
                    count++; //count of alive neighbors + current cell if alive
                }
            }
        }

        if (count < 2)
            *(u2 + y * columns + x) = 0;
        else if ((count == 2 || count == 3) && cell == 1)
            *(u2 + y * columns + x) = 1;
        else if (count > 3 && cell == 1)
            *(u2 + y * columns + x) = 0;
        else if (count == 3 && cell == 0)
            *(u2 + y * columns + x) = 1;
    }



    for (y = 1,x=1; y <= rows - 2; y++) {
        int pivot = y * columns + x;
        int cell = *(u1 + y * columns + x);

        int count = 0;
        for (i = -1; i <= +1; i++) {
            for (j = -1; j <= +1; j++) {
                if (i == 0 && j == 0) {
                    continue;
                }

                c = *(u1 + pivot + (i * columns) + j);

                if (c == 1) {
                    count++; //count of alive neighbors + current cell if alive
                }
            }
        }

        if (count < 2)
            *(u2 + y * columns + x) = 0;
        else if ((count == 2 || count == 3) && cell == 1)
            *(u2 + y * columns + x) = 1;
        else if (count > 3 && cell == 1)
            *(u2 + y * columns + x) = 0;
        else if (count == 3 && cell == 0)
            *(u2 + y * columns + x) = 1;
    }

    for (y = 1, x=columns-2; y <= rows - 2; y++) {
        int pivot = y * columns + x;
        int cell = *(u1 + y * columns + x);

        int count = 0;
        for (i = -1; i <= +1; i++) {
            for (j = -1; j <= +1; j++) {
                if (i == 0 && j == 0) {
                    continue;
                }

                c = *(u1 + pivot + (i * columns) + j);

                if (c == 1) {
                    count++; //count of alive neighbors + current cell if alive
                }
            }
        }

        if (count < 2)
            *(u2 + y * columns + x) = 0;
        else if ((count == 2 || count == 3) && cell == 1)
            *(u2 + y * columns + x) = 1;
        else if (count > 3 && cell == 1)
            *(u2 + y * columns + x) = 0;
        else if (count == 3 && cell == 0)
            *(u2 + y * columns + x) = 1;
    }
}


    /*----------------------- Reduce Functions ------------------------------*/
int gensEqual(int columns, int rows, int *u1, int *u2) {     //two consecutive generations are equal
    for (y = 1; y < rows - 1; y++)
        for (x = 1; x < columns - 1; x++)
            if (*(u2 + y * columns + x) - *(u1 + y * columns + x) != 0){
                return 1;
	    }
    return 0;
}

int genAllZero(int columns, int rows, int *u) {              //all cells are 0
    for (y = 1; y < rows - 1; y++)
        for (x = 1; x < columns - 1; x++)
            if (*(u + y * columns + x) == 1) {
                return 1;
            }
    return 0;
}


    /*----------------- Print Functions --------------------*/
void printFile(int columns, int rows, int *u1, char *filename) { // print data in file
    int y, x;
    FILE *fp;

    fp = fopen(filename, "w");
    for (y = 0; y < rows; y++) {
        for (x = 0; x < columns; x++) {
            fprintf(fp, "%d", *(u1 + y * columns + x));
            if (x != columns - 1)
                fprintf(fp, " ");
            else
                fprintf(fp, "\n");
        }
    }
    fclose(fp);
}

void printConsole(int columns, int rows, int *u) { // print data in console
    int y, x;

   printf("\n\n\n\n\n\n");

    for(y = 0; y < rows ; y++){
        for(x = 0; x < columns ; x++){
            printf("%d ", *(u + y*columns + x));
        }
        printf("\n");
    }
}

