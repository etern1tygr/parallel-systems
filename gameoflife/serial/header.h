/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   header.h
 * Author: etern1ty
 *
 * Created on December 23, 2017, 3:10 PM
 */

#ifndef HEADER_H
#define HEADER_H

#ifdef __cplusplus
extern "C" {
#endif




void begin(int rows, int columns);

char* create(int rows, int columns);

double getRandomDoubleInRange(double min, double max);

void display(int rows, int columns, char* simulation);

int countNeighborhood(int rows, int columns, int x, int y, char* simulation);

char* step(int rows, int columns, char* prevSimulation);



#ifdef __cplusplus
}
#endif

#endif /* HEADER_H */

