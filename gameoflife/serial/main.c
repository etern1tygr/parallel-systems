#include <stdlib.h>

#include <stdio.h>

#include <unistd.h>

#include "header.h"

#include <sys/time.h>

#include <time.h>

#define STEPS 300


 double get_time() { 
   struct timeval t; 
   gettimeofday(&t , NULL);
   double now = t.tv_sec + t.tv_usec/1000000.0; 
   return now;
}


void display(int rows, int columns, char* simulation)

{

    printf("\n\n\n\n\n\n");

    for(int y = 0; y < rows ; y++)

    {

        for(int x = 0; x < columns ; x++)

        {

            printf("%c ", *(simulation + y*columns + x));

        }

        printf("\n");

    }

}


int countNeighborhood(int rows, int columns, int x, int y, char* simulation)

{

    int count = 0;
    int pivot = y*columns+x;
    char c;
  //  printf("i am y= %d x= %d and my neighborhood is:\n",y,x);

    for(int i = -1; i <= +1; i++)
    {
        for(int j = -1; j <= +1; j++)
        {
            if(x==0 && j==-1){
                if(y==0 && i==-1)
                     c = *(simulation+pivot+(rows*columns)+j);
                else if(y==columns-1 && i==1)
                     c = *(simulation+pivot+2*columns-(rows*columns)+j);
                else
                     c = *(simulation+pivot+(i*columns)+columns+j);

            }
            else if(x==columns-1 && j==1){
                if(y==0 && i==-1)
                     c = *(simulation+pivot+(rows*columns)-2*columns+j);
                else if(y==columns-1 && i==1)    
                     c = *(simulation+pivot-(rows*columns)+j);
                else
                     c = *(simulation+pivot+(i*columns)-columns+j);
            }
            else if(y==0 && i==-1){
                 c = *(simulation+pivot+(rows*columns)-columns+j);
            }
            else if(y==columns-1 && i==1){
                 c = *(simulation+pivot-(rows*columns)+columns+j);
            }
            else{
                 c = *(simulation+pivot+(i*columns)+j);
            }
            
         //   printf("%c \n",c);
            if(c == '#') count++;
        }
    }

    return count;

}


/*

Any live cell with fewer than two live neighbors dies, as if caused by under-population.

Any live cell with two or three live neighbors lives on to the next generation.

Any live cell with more than three live neighbors dies, as if by over-population.

Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.

*/

char* step(int rows, int columns, char* prevSimulation)

{

    char* steppedSimulation = (char*) malloc(rows*columns*sizeof(int));

    if(steppedSimulation == NULL) return NULL;


    for(int y = 0; y < rows ; y++)

    {

        for(int x = 0; x < columns ; x++)

        {

            int live = countNeighborhood(rows,columns,x,y,prevSimulation);

            char cell = *(prevSimulation+y*columns+x);

            if(cell == '#') live--;

            *(steppedSimulation+y*columns+x) = cell;


            if(live < 2)

            {

                *(steppedSimulation+y*columns+x) = '.';

            }

            else if((live == 2 || live == 3) && cell == '#')

            {

                *(steppedSimulation+y*columns+x) = '#';

            }

            else if(live > 3 && cell == '#')

            {

                *(steppedSimulation+y*columns+x) = '.';

            }

            else if(live == 3 && cell == '.')

            {

                *(steppedSimulation+y*columns+x) = '#';

            }

        }

    }


    return steppedSimulation;

}


void begin(int rows, int columns)

{

    int i = 0;

   // printf("Columns Rows \n  %d %d \n",columns,rows);


    char* simulation = create(rows, columns);

    if(simulation == NULL) return;

  //  display(rows, columns, simulation);


    for(i=0;i < STEPS;i++)

    {

        char* newSim = step(rows,columns,simulation);

        printf("Step: %d  \n",i);

        if(newSim == NULL) return;


        free(simulation);

        simulation = newSim;

     //    display(rows,columns,simulation);

     // usleep(6000000);

    }


}


double getRandomDoubleInRange(double min, double max)

{

    return ((double)rand()/RAND_MAX)*(max-min)+min;

}


char* create(int rows, int columns)

{

    char* simulation = (char*)malloc(rows*columns*sizeof(char));

    if(simulation == NULL)
        return NULL;


    for(int y = 0; y < rows; y++)

    {

        for(int x = 0; x < columns; x++)

        {

            if(getRandomDoubleInRange(0.0,1.0) <= 0.35)

            {

                *(simulation + y*columns + x) = '#';

            }

            else

            {

                *(simulation + y*columns + x) = '.';

            }

        }

    }


    return simulation;

}


int main(int argc, char* argv[])

{
  srand(time(NULL));
  int rows = atoi(argv[1]);


  if(rows <= 0)

  {

    printf("Row count must be greater than zero. Was %d\n", rows);

    return -1;

  }

  //rows+=2;


  int columns = atoi(argv[2]);

  if(columns <= 0)

  {

    printf("Column count must be greater than zero. Was %d\n", columns);

    return -1;

  }

 // columns+=2;

  double start = 0, finish = 0, elapsed = 0;
  printf("Beginning! \n");
  start=get_time();


  begin(rows, columns);


  printf("Finished! \n");
  finish=get_time();
  elapsed = finish - start;
  printf("The code to be timed took %e seconds\n", elapsed);


}
